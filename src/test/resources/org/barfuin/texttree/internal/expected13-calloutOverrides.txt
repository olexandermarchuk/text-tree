ROOT
├─── A
│    ╰─── A2
│         ╰─── subTree (subtree annotation)
│              ├─── subA
│              │    ╰─── ‹depth exceeded›
│              ╰─── subB
├─── B
│    ├─── subTree ‹shown before›
│    ├─── B2
│    ╰─── ROOT ‹infinite loop›
╰─── C
     ├─── null
     ├─── null
     ╰─── C3
