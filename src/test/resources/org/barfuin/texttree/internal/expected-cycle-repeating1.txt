ROOT
+--- A
|    `--- A2
|         `--- subTree (subtree annotation)
|              +--- subA
|              |    +--- subA1
|              |    `--- subA2
|              `--- subB
+--- B
|    +--- subTree <shown before>
|    +--- B2
|    `--- ROOT <CYCLE>
`--- C
     +--- null
     +--- null
     `--- C3
